import React, { Component } from 'react';
import Slider from 'rc-slider';
import Tooltip from 'rc-tooltip';
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';
import './ColorSlider.css';

const Handle = Slider.Handle;

const sliderHandle = (props) => {
    const { value, dragging, index, ...restProps } = props;
    return (
        <Tooltip
            prefixCls="rc-slider-tooltip"
            overlay={value}
            visible={true}
            placement="top"
            key={index}
        >
            <Handle value={value} {...restProps} />
        </Tooltip>
    )
};

export default class ColorSlider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentValue: this.props.value,
            currentChannel: this.props.currentChannel
        };
        this.handleAfterChange = this.handleAfterChange.bind(this);
    };

    componentDidUpdate(prevProps) {
        if (prevProps.currentChannel === this.props.currentChannel && prevProps.value !== this.props.value)
            this.setState(Object.assign(this.state, { currentValue: this.props.value }));
        if (prevProps.currentChannel !== this.props.currentChannel) {
            this.setState(Object.assign(this.state, {currentChannel: this.props.currentChannel}));
            this.setState(Object.assign(this.state, {currentValue: this.props.value}));
        }
    };

    handleAfterChange = (val) => {
        this.setState(Object.assign(this.state, { currentValue: val }));
        this.props.onValueChange(this.props.currentChannel, val);
    };

    render() {
        return (
            <div className="color-slider-container">
                <Slider
                    min={0}
                    max={255}
                    defaultValue={255}
                    handle={sliderHandle}
                    value={this.state.currentValue}
                    onChange={newValue => this.setState(Object.assign(this.state, { currentValue: newValue }))}
                    onAfterChange={this.handleAfterChange}>
                </Slider>
                <p>Value: <strong>{this.state.currentValue}</strong></p>
                <p>Current channel: <strong><span className="channel">{this.state.currentChannel}</span></strong></p>
            </div>
        );
    };
}

