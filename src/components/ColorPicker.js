import React, { Component } from 'react';
import { SketchPicker } from 'react-color';
import './ColorPicker.css'

export default class ColorPicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rgb: this.props.rgb
        };
        this.handleChangeComplete = this.handleChangeComplete.bind(this);
    };

    handleChangeComplete = (color) => {
        this.setState(Object.assign(this.state, { rgb: color.rgb }));
        this.props.onColorChange(color);
    };

    componentDidUpdate(prevProps) {
        if (prevProps.rgb !== this.props.rgb)
            this.setState(Object.assign(this.state, {rgb: this.props.rgb}));
    };

    render() {
        return (
            <div className="color-picker-container">
                <SketchPicker color={this.props.rgb} disableAlpha={true} onChangeComplete={this.handleChangeComplete}></SketchPicker>
            </div>
        );
    };
}
