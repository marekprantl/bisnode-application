import React, { Component } from 'react';
import ColorPicker from './components/ColorPicker.js';
import ColorSlider from './components/ColorSlider.js';
import './App.css';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.appContainer = React.createRef();

    this.state = {
      currentChannel: "r",
      currentChannelValue: 255,
      rgb: {
        r: 255,
        g: 120,
        b: 30
      }
    };
    this.handlePickerChange = this.handlePickerChange.bind(this);
    this.handleSliderChange = this.handleSliderChange.bind(this);
  };

  componentDidMount() {
    // Background color change
    this.appContainer.current.style.background = this.getHexColor(this.state.rgb);
  };

  handlePickerChange = (color) => {
    // Color change
    this.setState(Object.assign(this.state, {rgb: color.rgb}));

    //Current channel and value change
    let channel = this.getHighestChannel(color.rgb);
    this.setState(Object.assign(this.state, { currentChannel: channel }));
    this.setState(Object.assign(this.state, { currentChannelValue: this.getChannelValue(channel) }));

    // Background color change
    this.appContainer.current.style.background = this.getHexColor(color.rgb);
  };

  handleSliderChange = (ch, val) => {
    // Color change
    let colors = { ...this.state.rgb };
    colors[ch] = val;
    this.setState(Object.assign(this.state, { rgb: colors }));

    // Current channel and value change
    let channel = this.getHighestChannel(colors);
    this.setState(Object.assign(this.state, { currentChannel: channel }));
    this.setState(Object.assign(this.state, { currentChannelValue: this.getChannelValue(channel) }));

    // Background color change
    this.appContainer.current.style.background = this.getHexColor(colors);
  };

  // Returns specified color in decimal to hex
  rgbToHex = (color) => {
    let hex = Number(color).toString(16);
    if (hex.length < 2) {
      hex = "0" + hex;
    }
    return hex;
  };

  // Converts RGB to Hex
  getHexColor = (rgb) => {
    let red = this.rgbToHex(rgb.r);
    let green = this.rgbToHex(rgb.g);
    let blue = this.rgbToHex(rgb.b);

    return "#" + red + green + blue;
  };

  // Returns a channel with the highest value
  // If channels have the same value function
  // returns values following the rule "R > G > B"
  getHighestChannel = (rgb) => {
    let red = rgb.r;
    let green = rgb.g;
    let blue = rgb.b;

    if (red > blue && red > green) return "r";
    if (green > red && green > blue) return "g";
    if (blue > red && blue > green) return "b";

    if (red === green && green === blue) return "r";
    else if (red === green && blue < green) return "r";
    else if (red === blue && green < blue) return "r";
    else if (green === blue && red < blue) return "g";
  };

  // Returns the value of the specified channel
  getChannelValue = (ch) => {
    let value = { ...this.state.rgb };
    value = value[ch];
    return value;
  };

  render() {
    return (
      <div className="App" ref={this.appContainer}>
        <div className="header-container text-center">
          <h1>Bisnode Application</h1>
        </div>
        <div className="content-container">
          <ColorPicker rgb={this.state.rgb} onColorChange={this.handlePickerChange}></ColorPicker>
          <ColorSlider rgb={this.state.rgb} currentChannel={this.state.currentChannel} value={this.state.currentChannelValue} onValueChange={this.handleSliderChange}></ColorSlider>
        </div>
      </div>
    );
  };
}
